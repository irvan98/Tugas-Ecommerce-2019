<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJenisTagihanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jenis_tagihan', function (Blueprint $table) {//ngasih nama tabel nya emang agak aneh, gak gw ubah krn takut error gak jelas.
            $table->bigIncrements('id');//primary key default dari laravel, gak gw ubah, takut error soalnya
            $table->string('deskripsiTagihan')->unique();//tagihan telkom / kuliah / listrik / internet / lainnya
            $table->timestamps();//ini juga entahlah, bawaan dari laravel juga
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jenis_tagihan');
    }
}
