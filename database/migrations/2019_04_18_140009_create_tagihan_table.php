<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagihanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tagihan', function (Blueprint $table) {
            $table->bigIncrements('id');//ini primary key default dari laravel, gw ga berani ubah namanya ,takut ada error ga jelas
            $table->integer('users_id');//tagihan ini punya user mana
            $table->bigInteger('besarTagihan');
            $table->dateTime('batasTgl');
            $table->integer('jenis_tagihan_id');//1 -> tagihan kuliah, 2-> tagihan telkom, 3-> tagihan lisrik
            $table->integer('instansi_id');
            //$table->string('deskripsiTagihan');;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tagihan');
    }
}
