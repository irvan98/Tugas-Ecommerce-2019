<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InstansiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('instansi')->insert([
            'namaInstansi' => 'Universitas Katolik Parahyangan'
        ]);
        DB::table('instansi')->insert([
            'namaInstansi' => 'Institut Teknologi Bandung'
        ]);
        DB::table('instansi')->insert([
            'namaInstansi' => 'Universitas Padjajaran'
        ]);
        DB::table('instansi')->insert([
        	'namaInstansi' => 'PLN'
        ]);
        DB::table('instansi')->insert([
        	'namaInstansi' => 'Telkom'
        ]);     
    }
}
