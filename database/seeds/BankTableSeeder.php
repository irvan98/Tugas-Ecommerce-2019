<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bank')->insert([
        	'namaBank' => 'BCA'
        ]);
        DB::table('bank')->insert([
        	'namaBank' => 'Mandiri'
        ]);
        DB::table('bank')->insert([
        	'namaBank' => 'BNI'
        ]);
        DB::table('bank')->insert([
        	'namaBank' => 'OCBC NISP'
        ]);
    }
}
