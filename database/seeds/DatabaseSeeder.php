<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BanksTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(InstansiTableSeeder::class);
        $this->call(JenisTagihanTableSeeder::class);
        $this->call(TagihanSeeder::class);
        $this->call(AdminsTableSeeder::class);
    }
}
