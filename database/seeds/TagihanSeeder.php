<?php

use Illuminate\Database\Seeder;

class TagihanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tagihan')->insert([
        	'history_pembayaran_id' => '0',
        	'users_id' => 1,
        	'besarTagihan' => '1000000',
        	'batasTgl' => '2019-04-28 00:00:00',
        	'jenis_tagihan_id' => '1',
        	'instansi_id' => '1',
        ]);
        DB::table('tagihan')->insert([
        	'history_pembayaran_id' => '0',
        	'users_id' => 1,
        	'besarTagihan' => '1000000',
        	'batasTgl' => '2019-04-28 00:00:00',
        	'jenis_tagihan_id' => '2',
        	'instansi_id' => '2',
        ]);
        DB::table('tagihan')->insert([
        	'history_pembayaran_id' => '0',
        	'users_id' => 1,
        	'besarTagihan' => '1000000',
        	'batasTgl' => '2019-04-28 00:00:00',
        	'jenis_tagihan_id' => '3',
        	'instansi_id' => '3',
        ]);
    }
}
