<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;//koneksi ke database
use Illuminate\Support\Facades\Hash;//untuk password

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
        	'username' => 'john97',
        	'password' => Hash::make('123456789'),
        	'nama' => 'John Doe'
        ]);       
    }
}
