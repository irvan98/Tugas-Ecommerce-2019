<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class JenisTagihanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jenis_tagihan')->insert([
            'deskripsiTagihan' => 'Tagihan Kuliah'
        ]);
        DB::table('jenis_tagihan')->insert([
        	'deskripsiTagihan' => 'Tagihan Listrik'
        ]);
        DB::table('jenis_tagihan')->insert([
        	'deskripsiTagihan' => 'Tagihan Telkom'
        ]);
    }
}
