<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;//koneksi ke database
use Illuminate\Support\Facades\Hash;//untuk password

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'username' => 'agus123',
        	'password' => Hash::make('agusganteng'),
        	'full_name' => 'Agus Sudarto',
        	'cc_number' => '123456789',
        	'email' => 'tempat.k.b.c.150@gmail.com',
        	'bank_id' => '2',
        	'start_date' => '2019-04-21',
        	'end_date' => '2020-04-21',
            'otomatis' => '0',
        ]);
        DB::table('users')->insert([
        	'username' => 'budimoetz',
        	'password' => Hash::make('budiunch'),
        	'full_name' => 'Budi Suhendar',
        	'cc_number' => '012302123',
        	'email' => 'sefriel.gresyel@gmail.com',
        	'bank_id' => '1',
        	'start_date' => '2019-02-21',
        	'end_date' => '2020-03-21',
            'otomatis' => '0',
        ]);
        DB::table('users')->insert([
        	'username' => 'cecep234',
        	'password' => Hash::make('cecepgaulz'),
        	'full_name' => 'Cecep Wijaya',
        	'cc_number' => '008080823',
        	'email' => 'alvinussutendy@gmail.com',
        	'bank_id' => '3',
        	'start_date' => '2019-01-21',
        	'end_date' => '2020-02-12',
            'otomatis' => '0',
        ]);
    }
}
