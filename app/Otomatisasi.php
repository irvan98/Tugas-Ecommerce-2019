<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Otomatisasi extends Model
{
    protected $table = "otomatisasi";
    protected $fillable =['users_id','jenis_tagihan_id'];

    public function user(){
    	return $this->hasMany(User::class);
    }

    public function jenisTagihan(){
    	return $this->hasMany(JenisTagihan::class);
    }
}
