<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instansi extends Model
{
    protected $table = "instansi";
    protected $fillable =['namaInstansi'];

    public function history(){
    	return $this->hasMany(Tagihan::class);
    }
}

