<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{	
	protected $table = "Bank";
    protected $fillable=['namaBank'];

    public function user(){
    	return $this->hasMany(User::class);
    }


}
