<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisTagihan extends Model
{
    protected $table = "jenis_tagihan";
    protected $fillable=['deskripsiTagihan'];

    public function otomatisasi(){
        return $this->belongsTo(Otomatisasi::class);
    }
}
