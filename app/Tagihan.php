<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tagihan extends Model
{	
	protected $table = "tagihan";
    protected $fillable=['besarTagihan','users_id','batasTgl','jenis_tagihan_id','instansi_id, history_pembayaran_id'];

    public function user(){
    	return $this->belongsTo(User::class);
    }

    public function history(){
    	return $this->hasOne(HistoriPembayaran::class);
    }

    public function instansi(){
    	return $this->belongsTo(Instansi::class); 
    }
}
