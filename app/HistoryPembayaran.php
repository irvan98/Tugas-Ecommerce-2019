<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryPembayaran extends Model
{
	protected $table = "history_pembayaran";
    protected $fillable=['users_id','tagihan_id','tglBayar'];

     public function tagihan(){
    	return $this->belongsTo(Tagihan::class);
    }

    public function users(){
    	return $this->belongsTo(User::class);
    }
}
