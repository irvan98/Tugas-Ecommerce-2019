<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use App\Mail\KonfirmasiMailAddTagihan;
use App\Mail\KonfirmasiRegister;
use App\User;
use Illuminate\Support\Facades\Auth;

class MailController extends Controller
{
    public function sendMail()
    {
        // $record = User::where('id', \Auth::user()->id)->get(); 
        $email = User::where('id', \Auth::user()->id)->get('email'); 
        $tempNama = User::where('id', \Auth::user()->id)->get('full_name'); 
        $name = $tempNama[0]['full_name'];

        Mail::to($email)->send(new SendMailable($name));
        
        return view('Pembayaran/pembayaranSukses');
        // return $name;
    }

    public function confirmationAddTagihanMail()
    {
        $email = User::where('id', \Auth::user()->id)->get('email'); 
        $tempNama = User::where('id', \Auth::user()->id)->get('full_name'); 
        $name = $tempNama[0]['full_name'];

        Mail::to($email)->send(new KonfirmasiMailAddTagihan($name));        
    }

    public function confirmationRegister($email, $name)
    {
        // $email = User::where('id', \Auth::user()->id)->get('email'); 
        // $tempNama = User::where('id', \Auth::user()->id)->get('full_name'); 
        // $name = $tempNama[0]['full_name'];

        Mail::to($email)->send(new KonfirmasiRegister($name));        
    }
}
