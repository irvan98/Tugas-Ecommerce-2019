<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Model\Donation;
use Veritrans_Config;
use Veritrans_Snap;
use Veritrans_Notification;
use App\Tagihan;
use App\HistoryPembayaran;
 
class DonationController extends Controller
{
    /**
     * Make request global.
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;
    protected $besarTagihan;
 
    /**
     * Class constructor.
     *
     * @param \Illuminate\Http\Request $request User Request
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
 
        // Set midtrans configuration
        Veritrans_Config::$serverKey = config('services.midtrans.serverKey');
        Veritrans_Config::$isProduction = config('services.midtrans.isProduction');
        Veritrans_Config::$isSanitized = config('services.midtrans.isSanitized');
        Veritrans_Config::$is3ds = config('services.midtrans.is3ds');
    }
 
    /**
     * Show index page.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $data['donations'] = Donation::orderBy('id', 'desc')->paginate(8);
 
        return view('pembayaran', $data);
    }
 
    /**
     * Submit donation.
     *
     * @return array
     */
    public function submitDonation()
    {   
        

        \DB::transaction(function(){
            // Save donasi ke database
            $donation = Donation::create([
                'donor_name' => $this->request->donor_name,
                'donor_email' => $this->request->donor_email,
                'tagihan_id' => $this->request->tagihan_id,
                'besar_tagihan' => $this->request->besar_tagihan,
                'amount' => $this->request->besar_tagihan,
            ]);
 
            // Buat transaksi ke midtrans kemudian save snap tokennya.
            $payload = [
                'transaction_details' => [
                    'order_id'      => $donation->id,
                    'gross_amount'  => $donation->amount,
                ],
                'customer_details' => [
                    'first_name'    => $donation->donor_name,
                    'email'         => $donation->donor_email,
                    // 'phone'         => '08888888888',
                    // 'address'       => '',
                ],
                'item_details' => [
                    [
                        'id'       => $donation->tagihan_id,
                        'price'    => $donation->amount,
                        'quantity' => 1,
                        'name'     => ucwords(str_replace('_', ' ', $donation->tagihan_id))
                    ]
                ]
            ];
            $snapToken = Veritrans_Snap::getSnapToken($payload);
            app('App\Http\Controllers\HistoryController')->savePayment($this->request);
            $donation->snap_token = $snapToken;
            $donation->save();

            // Beri response snap token
            $this->response['snap_token'] = $snapToken;
        });
 
        return response()->json($this->response);
    }
}
 