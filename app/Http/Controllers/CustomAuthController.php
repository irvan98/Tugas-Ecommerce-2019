<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Bank;
use App\Tagihan;

class CustomAuthController extends Controller
{
    public function showRegisterForm(){
        $bank = Bank::all();
    	return view('register', compact('bank'));
    }

    public function showLoginForm(){
    	return view('login');
    }

    public function register(Request $request){
    	$this->validateUserData($request);
        $this->insertData($request);
        $tempNama = request('full_name'); 
        $name = $tempNama[0]['full_name'];
        app('App\Http\Controllers\MailController')->confirmationRegister(request('email'),$name);

    	return redirect('/welcome')->with('success','Hooray! You are registered!');
    }

    public function validateUserData(Request $request){
    	return $this->validate($request, [
    			'username' => 'required|max:255',
    			'password' => 'required|confirmed|min:8',
    			'fullname' => 'required|max:255',
    			'cc_number' => 'required|max:255',
    			'email' => 'required|email',
    			'bank_id' => 'required',
    			'start_date' => 'required',
    			'end_date' => 'required'
    		]);
    }

    public function insertData(Request $request){
        $user = new User([
            'username' => $request->get('username'),
            'password' => Hash::make($request->get('password')),
            'full_name' => $request->get('fullname'),
            'cc_number' => $request->get('cc_number'),
            'email' => $request->get('email'),
            'bank_id' => $request->get('bank_id'),
            'start_date' => $request->get('start_date'),
            'end_date'=> $request->get('end_date'),
            'otomatis' => '0',
        ]);

        $user->save();
    }

    public function showWelcome(){
        $timenow = \Carbon\Carbon::now();

        $tagihan = Tagihan::where('users_id', \Auth::user()->id)
                            ->where('history_pembayaran_id', 0)
                            ->get();
        return view('welcome', compact('tagihan','timenow'));
    }

    public function checkLogin(Request $request){
        $this->validate($request,[
            'username' => 'required|max:255',
            'password' => 'required|min:8'
        ]);

        $user_data = [
            'username' => $request->get('username'),
            'password' => $request->get('password')
        ];
        
        if(Auth::attempt($user_data)){
            return redirect('/welcome');
        }else{
            return back()->with('error','Wrong login information');
        }
    }

    public function logout(){
        Auth::logout();
        return redirect('login');
    }

    public function username()
    {
        return 'username';
    }
}
