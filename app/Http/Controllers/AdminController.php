<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Instansi;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function checkLoginAdmin(Request $request){
    	 $admin_data = [
            'username' => $request->get('username'),
            'password' => $request->get('password')
        ];

        if(Auth::guard('admin')->attempt($admin_data)){
            return redirect('/berandaadmin');
        }else{
            return back()->with('error','Wrong login information');
        }
    }

    public function logoutAdmin(){
    	Auth::guard('admin')->logout();
    	return redirect('/loginadmin');
    }

    public function showInstansi(){
        $data['instansi'] = DB::table('instansi')->get();
        return view('Admin/listinstansi',compact('data'));   
    }

    public function insertInstansi(Request $request){
        $instansi = new Instansi([
            'namaInstansi' => $request->get('namaInstansi'),
        ]);
        $instansi->save();
        return redirect('/listinstansi')->with('success','Instansi successfuly added!');
    }

    public function updateInstansi(Request $request){
        $instansi = Instansi::where('id',$request->get('id'))->update(['namaInstansi' => $request->get('newName')]);
        return redirect('/listinstansi')->with('info','Instansi successfuly changed');
    }

    public function deleteInstansi(Request $request){

    }
    public function username()
    {
        return 'username';
    }
}
