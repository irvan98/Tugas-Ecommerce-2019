<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tagihan;
use App\JenisTagihan;
use App\Instansi;
use App\Http\Controllers\MailController;

class TambahTagihan extends Controller
{
    // id, history_pembayaran_id, users_id, besarTagihan, batasTgl, jenis_tagihan_id, instansi_id, 
    //             created_at, updated_at
    public function addTagihan(Request $request){
        $history_pembayaran_id = 0;
        $users_id = \Auth::user()->id;
        $besarTagihan = $request->get('besarTagihan');
        $batasTgl = $request->get('batasTgl');
        $jenis_tagihan_id = $request->get('jenis_tagihan_id');
        $instansi_id = $request->get('instansi_id');

        $tagihanBaru = new Tagihan();
        $tagihanBaru->history_pembayaran_id = $history_pembayaran_id;
        $tagihanBaru->users_id = $users_id;
        $tagihanBaru->besarTagihan = $besarTagihan;
        $tagihanBaru->batasTgl = $batasTgl;
        $tagihanBaru->jenis_tagihan_id = $jenis_tagihan_id;
        $tagihanBaru->instansi_id = $instansi_id;
        
        $tagihanBaru->save();
        // confirmationAddTagihanMail();
        app('App\Http\Controllers\MailController')->confirmationAddTagihanMail();

        return redirect('tagihanBaru2');
    }

    public function sendFormData(){
        $jenisTagihan = JenisTagihan::all();
        $instansi = Instansi::all();

        return view('tagihanBaru', compact('jenisTagihan','instansi'));
    }
}
