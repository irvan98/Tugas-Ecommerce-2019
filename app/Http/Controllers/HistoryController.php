<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\HistoryPembayaran;
use App\Tagihan;
use Carbon\Carbon;

class HistoryController extends Controller
{
    public function showHistory(){
        $history = HistoryPembayaran::where('users_id', \Auth::user()->id)->get(); 
    	return view('histori',compact('history'));
    }

     public function savePayment(Request $request){
     	$mytime = Carbon::now();
     	$tagihan_id = $request->get('tagihan_id');

         $history = new HistoryPembayaran([
            'users_id' => \Auth::user()->id,
            'tagihan_id' => $tagihan_id,
            'tglBayar' => $mytime
        ]);

        $history->save();

        $tagihan = Tagihan::where('id', $tagihan_id)
                            ->update(['history_pembayaran_id' => $history->id]);
        // return redirect(route('paySuccess'));
    }

    public function success(){
        // return view('Pembayaran/pembayaranSukses');
        return redirect(route('sendEmail'));
    }
}
