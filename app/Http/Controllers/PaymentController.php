<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\HistoryPembayaran;
use App\Tagihan;
use App\User;

class PaymentController extends Controller
{

	public function showPayment(Request $request){
		return view('Pembayaran/pembayaran')->with('idTagihan',$request->tagihan_id);
	}
    public function showPaymentSchool(Request $request){
        $sisaWaktu = $request->sisaWaktu;
        $idTagihan = $request->tagihan_id;
    	return view('Pembayaran/pembayaranSekolah',compact('idTagihan','sisaWaktu'));
    }

    public function showPaymentElectric(Request $request){
        $sisaWaktu = $request->sisaWaktu;
        $idTagihan = $request->tagihan_id;
    	return view('Pembayaran/pembayaranListrik',compact('idTagihan','sisaWaktu'));  
    }

    public function showPaymentTelephone(Request $request){
        $sisaWaktu = $request->sisaWaktu;
        $idTagihan = $request->tagihan_id;
    	return view('Pembayaran/pembayaranTelepon',compact('idTagihan','sisaWaktu'));
    }

    public function setAutomatic(Request $request){
        $timenow = \Carbon\Carbon::now();

        $user = User::where('id', \Auth::user()->id)
                            ->update(['otomatis' => $request->onoff]);
        $userBaru = User::where('id', \Auth::user()->id)->get();
        $tagihan = Tagihan::where('users_id', \Auth::user()->id)
                            ->where('history_pembayaran_id', 0)
                            ->get();
        //return redirect('welcome', compact('tagihan','timenow', 'onoff'));
        return redirect('\welcome');
    }
}
