<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class User extends Model implements AuthenticatableContract
{
    use Authenticatable;
    protected $fillable = ['username','password','full_name','cc_number','email','bank_id','start_date','end_date', 'otomatis'];

    protected $hidden = [
        'password','remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function bank(){
    	return $this->belongsTo(Bank::class);
    }

    public function tagihan(){
    	return $this->hasMany(Tagihan::class);
    }

    public function history(){
        return $this->hasMany(HistoriPembayaran::class);
    }

    public function otomatisasi(){
        return $this->belongsTo(Otomatisasi::class);
    }
}
