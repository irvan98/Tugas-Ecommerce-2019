<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','CustomAuthController@showWelcome')->name('landingpage')->middleware('auth');
Route::get('login','CustomAuthController@showLoginForm')->name('login');
Route::get('/logout','CustomAuthController@logout');
Route::get('custom-register','CustomAuthController@showRegisterForm')->name('asd');
Route::post('custom-register','CustomAuthController@register')->name('register');
Route::get('/welcome','CustomAuthController@showWelcome')->name('landingpage')->middleware('auth');
Route::post('/checklogin','CustomAuthController@checkLogin')->name('/checklogin');
Route::get('/payment', 'PaymentController@showPayment')->name('pay');
Route::get('/reminder','ReminderController@showReminder')->name('remind');
Route::get('/paymentSchool','PaymentController@showPaymentSchool');
Route::get('/paymentElectric','PaymentController@showPaymentElectric');
Route::get('/paymentTelephone','PaymentController@showPaymentTelephone');
Route::get('/history','HistoryController@showHistory')->name('history');
Route::get('/berita','BeritaController@showBerita')->name('berita');
Route::post('/add-payment', 'HistoryController@savePayment')->name('addPayment');
Route::get('/add-payment', 'HistoryController@success')->name('paySuccess');

Route::get('/customerService', function () {
    return view('CustomerService/customerService');
});

Route::get('/help', function () {
    return view('help');
});

//routes khusus admin
Route::get('/loginadmin', function(){
	return view('Admin/loginadmin');
});

Route::get('/berandaadmin',function(){
	return view('Admin/home');
});

Route::post('/checkloginadmin','AdminController@checkLoginAdmin')->name('/checkloginadmin');

Route::get('/logoutadmin','AdminController@logoutAdmin');

Route::get('/listinstansi','AdminController@showInstansi');

Route::get('/tambahinstansi',function(){
	return view('Admin/tambahinstansi');
});

Route::post('/insertinstansi','AdminController@insertInstansi')->name('/insertinstansi');

Route::get('/editinstansi',function(){
	return view('Admin/editinstansi');
});

Route::post('/updateinstansi','AdminController@updateInstansi')->name('/updateinstansi');

Route::get('/sendEmail', 'MailController@sendMail')->name('sendEmail');

//Midtrans
Route::get('/pembayaran', 'DonationController@index')->name('welcome');
Route::post('/finish', function(){
    return redirect()->route('welcome');
})->name('donation.finish');
 
Route::post('/donation/store', 'DonationController@submitDonation')->name('donation.store');

Route::get('/setting', 'pengaturanController@autoOff')->name('autoOff');

Route::get('/autoPayOff', 'paymentController@setAutomatic')->name('autoPayOnOff');

Route::get('/tagihanBaru', function(){
    return view('tagihanBaru');
})->name('tagihanBaru');

Route::get('/tagihanBaru2', 'TambahTagihan@sendFormData')->name('tagihanBaru2');
Route::post('/tagihanBaru3', 'TambahTagihan@addTagihan')->name('tambahTagihan');

