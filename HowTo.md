# Edit register step:
1. Buat view form register
1. Buat model dan migration dengan sintaks: ```php artisan make:model User --migration```
2. ubah function up() pada ```2014_10_12_100000_create_users_table.php```
```php
public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username')->unique();
            $table->string('password');
            $table->string('full_name');
            $table->string('cc_number')->unique();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->tinyInteger('bank_id');
            $table->date('start_date');
            $table->date('end_date');
            $table->rememberToken();
            $table->timestamps();
        });
    }
```
3. run migration dengan sintaks :```php artisan migrate```
4. Buat function untuk menerima data register pada controller
```php
public function register(Request $request){
    	$this->validateUserData($request);
        $this->insertData($request);
    	return redirect('/welcome')->with('success','Hooray! You are registered!');
    }

    public function validateUserData(Request $request){
    	return $this->validate($request, [
    			'username' => 'required|max:255',
    			'password' => 'required|confirmed|min:8',
    			'fullname' => 'required|max:255',
    			'cc_number' => 'required|max:255',
    			'email' => 'required|email',
    			'bank_id' => 'required',
    			'start_date' => 'required',
    			'end_date' => 'required'
    		]);
    }

    public function insertData(Request $request){
        $user = new User([
            'username' => $request->get('username'),
            'password' => Hash::make($request->get('password')),
            'full_name' => $request->get('fullname'),
            'cc_number' => $request->get('cc_number'),
            'email' => $request->get('email'),
            'bank_id' => $request->get('bank_id'),
            'start_date' => $request->get('start_date'),
            'end_date'=> $request->get('end_date')
        ]);

        $user->save();
    }
```
5. Buat route baru untuk memanggil function **register** pada web.php
```php
Route::post('custom-register','CustomAuthController@register')->name('register');
```

6. tambahkan atribut **action** pada form
```php
<form method="POST" action="{{ route('register') }}">
```

# Edit login step:
1. pastikan file **User.php** ada di direktori **app/User.php**
2. Ubah file **User.php** menjadi:
```php
use Illuminate\Database\Eloquent\Model;//Default dari laravel
use Illuminate\Auth\Authenticatable;//agar tidak error saat Auth::attempt
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;//agar tidak error saat Auth::attempt

class User extends Model implements AuthenticatableContract//agar tidak error saat Auth::attempt
{
    use Authenticatable;//agar tidak error saat Auth::attempt
    //kode lainnya
}
```

3. Siapkan view yang berisi form login
4. Buat Controller Baru
5. By default, laravel menggunakan email dan password untuk login. Agar bisa login menggunakan username dan password, tambahkan kode ini diawal Controller
```php
use Illuminate\Foundation\Auth\AuthenticatesUsers;
```
lalu override function **username()** (pada Controller yang baru dibuat)
```php
public function username()
    {
        return 'username';//awalnya return 'email'
    }
```
6. Buat sebuah function untuk menerima data login
```php
public function checkLogin(Request $request){
        $this->validate($request,[
            'username' => 'required|max:255',//memeriksa apakah username tidak kosong dan panjangnya tidak lebih dari 255 karakter.
            'password' => 'required|min:8'//memeriksa apakah password tidak kosong dan panjangnya >= 8 karakter.
        ]);

        $user_data = [
            'username' => $request->get('username'),//ambil username nya
            'password' => $request->get('password')//ambil password nya
        ];
        
        if(Auth::attempt($user_data)){//coba login dengan pasangan username dan password
            return redirect('/welcome');//tampilkan halaman welcome
        }else{
            return back()->with('error','Wrong login information');//kembali ke halaman login dengan pesan error
        }
    }
```
**keterangan:** function **Auth::attempt()** akan mencari apakah username dan password yang diinput user ada di database. jika ada, function ini akan me-return true,
dan menset sebuah session baru. Sebaliknya, return false. Jika logout, matikan session dengan **Auth::logout()**

7. panggil function checkLogin pada **web.php**
```php
Route::post('/checklogin','CustomAuthController@checkLogin')->name('/checklogin');
```
**keterangan**: kalo di gue sih nama controllernya CustomAuthController 

8. tambahkan atribut action pada form
```php
<form method="POST" action="{{ route('/checklogin') }}">
```