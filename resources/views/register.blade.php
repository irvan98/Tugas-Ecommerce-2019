<!DOCTYPE html>
<html>
<head>
    <title>Sign Up - Auto Payment</title>
    @include('template.header')
</head>
<body style="background-image:url(images/4.jpg);background-size:cover">
    <div class="container" style="height: 100vh">
        <div class="row align-items-center justify-content-center" style="height:100vh">
            <div class="col-7 p-5" style="background-color: rgba(36, 30, 30, 0.51);height: 80vh;overflow-y: auto">
                <?php
                if (count($errors) > 0){
                    $str="";
                    foreach ($errors->all() as $key => $value) {
                        $str.=$value."<br>";
                    }
                    echo("<p class='alert alert-danger'>".$str."</p>");
                }
                ?>
                <form method="POST" action="{{ route('register') }}">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label class="text-light" for="username">Username</label>
                        <input type="text" name="username" value="{{old('username')}}" class="form-control" placeholder="username">
                    </div>
                    <div class="form-group">
                        <label class="text-light "for="password">Password</label>
                        <input type="password" name = "password" class="form-control" placeholder="password">
                    </div>
                    <div class="form-group">
                        <label class="text-light "for="confirmpassword">Confirm Password</label>
                        <input type="password" name = "password_confirmation" class="form-control" placeholder="confirm password">
                    </div>
                    <div class="form-group">
                        <label class="text-light "for="name">Name</label>
                        <input type="text" name="fullname" value="{{old('fullname')}}" class="form-control" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <label class="text-light "for="creditcardno">Credit Card Number</label>
                        <input type="text" name="cc_number" value="{{old('cc_number')}}" class="form-control" placeholder="Credit Card Number">
                    </div>
                    <div class="form-group">
                        <label class="text-light "for="email">E-mail address</label>
                        <input type="email" name="email" value="{{old('email')}}" class="form-control" placeholder="E-mail address">
                    </div>
                    <div class="form-group-">
                        <label class="text-light "for="bank_id">Bank Name</label>
                        <select name ="bank_id" value="{{old('bank_id')}}" class="form-control">
                            @if ($bank->count() > 0)
                                @foreach ($bank as $banks)
                                    <option value="{{$banks->id}}">{{ $banks->namaBank }}</option>
                                @endforeach
                            @else
                                  <option></option>
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="text-light">Batas waktu</label>
                        <div>
                            <div class="d-inline-block">
                                <input type="date" name="start_date" value="{{old('start_date')}}" placeholder="Start">
                            </div>
                            <div class="d-inline-block">
                                <label class="text-light">hingga</label>
                            </div>
                            <div class="d-inline-block">
                                <input type="date" name="end_date" value="{{old('end_date')}}" placeholder="End">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Sign Up</button>
					<a href="{{route('landingpage')}}" class="btn btn-primary">Cancel</a>
                </form>
            </div>
        </div>
    </div>
</body>
</html>       