<!DOCTYPE html>
<html>

<head>
	<title>Reminder - Auto Payment</title>
	@include('template.header')
</head>

<body style="background-image:url(images/4.jpg);background-size:cover">
	<div class="container" style="height: 100vh">
		<div class="row align-items-center justify-content-center" style="height:100vh">
    		<div class="col-10 p-5" style="background-color: rgba(36, 30, 30, 0.51)">
				<div class="row">
						<h1 class="text-light">| Reminder</h1>
					<div class="col p-4 align-items-center">
						<div class="row justify-content-end">
							@include('template.navbar')
						</div>
					</div>
				</div>

				<ul class="list-group p-5">
					<a href="#" class="list-group-item list-group-item-action list-group-item-danger">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">Tagihan Telkom</h5>
							<small>3 hari lagi</small>
						</div>
						<p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
						<small>Donec id elit non mi porta.</small>
					</a>
					<a href="#" class="list-group-item list-group-item-action">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">Tagihan Kuliah</h5>
							<small>10 hari lagi</small>
						</div>
						<p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
						<small>Donec id elit non mi porta.</small>
					</a>
					<a href="#" class="list-group-item list-group-item-action">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">Tagihan Listrik</h5>
							<small>12 hari lagi</small>
						</div>
						<p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
						<small>Donec id elit non mi porta.</small>
					</a>
				</ul>
    		</div>
		</div>
	</div>
</body>

</html>