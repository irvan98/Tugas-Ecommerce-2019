<!DOCTYPE html>
<html>
<head>
	<title>Control Panel</title>
	@include('template.header')
</head>
<body style="background-image:url(/images/4.jpg);background-size:cover">
	<div class="container" style="height:100vh">
		<div class="row align-items-center justify-content-center" style="background-color: rgba(36, 30, 30, 0.51);height:20vh">
			<div class="col p-5" style="background-color: rgba(36, 30, 30, 0.51);height:20vh">
				<h1 class="text-light">Daftar Instansi</h1>
			</div>
			<div class="col p-5 text-right" style="background-color: rgba(36, 30, 30, 0.51);height:20vh">
				<select>
					<option value="1">Instansi</option>
					<option value="2">Jenis Tagihan</option>
				</select>
				<a class="btn btn-primary text-light" href="{{url('/logoutadmin')}}">Keluar</a>
				<a class="btn btn-primary text-light" href="{{url('/berandaadmin')}}">Beranda</a>
			</div>
		</div>
		<div class="row align-items-center justify-content-center" style="background-color: rgba(36, 30, 30, 0.51);height:70vh;overflow-y: auto">
			<div class="col p-5">
				@if(session('success'))
                    <p class="alert alert-success">{{session('success')}}</p>
                @elseif(session('info'))
                	<p class="alert alert-info">{{session('info')}}</p>
                @endif
				<table class="table table-dark">
					<thead>
						<tr>
							<th scope="col">No</th>
							<th scope="col">Instansi</th>
							<th scope="col">Opsi</th>
						</tr>
					</thead>
					<tbody>
						@foreach($data['instansi'] as $value)
						<tr>
							<th scope="row">{{ $value->id }}</th>
							<td>{{ $value->namaInstansi }}</td>
							<td>
								<a href="{{url('/editinstansi?id='.$value->id.'&namaInstansi='.$value->namaInstansi)}}" class="btn btn-primary">Edit</a>
								<a href="#" class="btn btn-primary">Hapus</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<div class="row" style="height:10vh">
			<div class="col p-2">
				<a class="btn btn-primary text-light" href="{{url('/tambahinstansi')}}">Tambah Instansi</a>
			</div>
		</div>
	</div>
</body>
</html>