<!DOCTYPE html>
<html>
<head>
	<title>Control Panel</title>
	@include('template.header')
</head>
<body style="background-image:url(/images/4.jpg);background-size:cover">
	<div class="container" style="height:100vh">
		<div class="row align-items-center justify-content-center" style="background-color: rgba(36, 30, 30, 0.51);height:20vh">
			<div class="col p-5" style="background-color: rgba(36, 30, 30, 0.51);height:20vh">
				<h1 class="text-light">Ubah Nama Instansi</h1>
			</div>
			<div class="col p-5 text-right" style="background-color: rgba(36, 30, 30, 0.51);height:20vh">
				<a class="btn btn-primary text-light" href="{{url('/logoutadmin')}}">Keluar</a>
				<a class="btn btn-primary text-light" href="{{url('/berandaadmin')}}">Beranda</a>
			</div>
		</div>
		<div class="row align-items-center justify-content-center" style="background-color: rgba(36, 30, 30, 0.51);height:80vh">
			<div class="col-6">
				<form method="POST" action="{{route('/updateinstansi')}}">
					{{ csrf_field() }}
					<div class="form-group">
						<label class="text-light">Id Instansi</label>
						<input type="number" name="id" value="{{$_GET['id']}}" class="form-control" readonly>
					</div>
					<div class="form-group">
						<label class="text-light">Nama Sebelumnya</label>
						<input type="text" name="oldName" value="{{$_GET['namaInstansi']}}" class="form-control" readonly>
					</div>
					<div class="form-group">
						<label class="text-light">Nama Instansi Baru</label>
						<input type="text" class="form-control" name="newName" placeholder="Nama Instansi Baru">
					</div>
					<button type="submit" class="btn btn-primary">Simpan Perubahan</button>
				</form>
			</div>
		</div>
	</div>
</body>
</html>