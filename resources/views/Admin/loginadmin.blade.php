<!DOCTYPE html>
<html>
	<head>
		<title>Administrator - Auto Payment</title>
		@include('template.header')
	</head>
	<body style="background-image:url(/images/4.jpg);background-size:cover">
		<div class="container" style="height:100vh">
			<div class="row align-items-center justify-content-center" style="height:100vh">
				<div class="col-5 p-3" style="background-color: rgba(36, 30, 30, 0.51)">
					<h1 class="text-light">ADMIN PORTAL</h1>
					<p class="alert alert-danger">WARNING! for admin only</p>
					<form method="POST" action="{{route('/checkloginadmin')}}">
						{{ csrf_field() }}
						<div class="form-group">
							<label class="text-light">Username</label>
							<input type="text" class="form-control" name="username" placeholder="username">
						</div>
						<div class="form-group">
							<label class="text-light">Password</label>
							<input type="password" class="form-control" name="password" placeholder="password">
						</div>
						<button type="submit" class="btn btn-primary">Login As Administrator</button>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>