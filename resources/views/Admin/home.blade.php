<!DOCTYPE html>
<html>
<head>
	<title>Control Panel</title>
	@include('template.header')
</head>
	<body style="background-image:url(/images/4.jpg);background-size:cover">
		<div class="container" style="height:100vh">
			<div class="row align-items-center justify-content-center" style="height:20vh">
				<div class="col p-5" style="background-color: rgba(36, 30, 30, 0.51);height:20vh">
					<h1 class="text-light">Control Panel</h1>
				</div>
				<div class="col p-5 text-right" style="background-color: rgba(36, 30, 30, 0.51);height:20vh">
					<a class="btn btn-primary text-light" href="{{url('/logoutadmin')}}">Keluar</a>
				</div>
			</div>
			
			<div class="row align-items-center justify-content-center" style="background-color: rgba(36, 30, 30, 0.51);height:80vh">
				<div class="col-6">
					<a class="btn btn-primary text-light" href="{{url('/listinstansi')}}">List Instansi</a>
					<a class="btn btn-primary text-light">List Jenis Tagihan</a>
					<a class="btn btn-primary text-light">List Histori Pembayaran</a>
				</div>
			</div>
		</div>
	</body>
</html>