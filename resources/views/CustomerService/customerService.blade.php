<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Chat Customer Service</title>

    <!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->

    <!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="{{ asset('css/cs.css') }}" rel="stylesheet">
    <script src="{{ asset('js/cs.js') }}"></script>
</head>
<body>

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">

            <div class="navbar-header">
                <a class="navbar-brand">Customer Service</a>
            </div>

            <ul class="nav navbar-nav">
                <!-- <li class="active"><a href="#">Home</a></li> -->
                <li><a href="\welcome"><span class="glyphicon glyphicon-home"></span> Home</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#"><span class="glyphicon glyphicon-bell"></span> Notification</a></li>
                <li><a href="{{url('/logout')}}"><span class="glyphicon glyphicon-log-in"></span> Sign Out</a></li>
            </ul>

        </div>
    </nav>

    <div class="jumbotron">
        <div class="container text-center">
            <!-- <img src="../images/cs.png" width="21%" height="30%">     -->
            <div class="container">
                <h2>With Care</h2>  
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <img class="img-responsive" src="../images/cs4.png" style="width:25%; height: 25%">
                        </div>

                        <div class="item">
                            <img class="img-responsive" src="../images/cs5.png"  style="width:25%; height: 25%" >
                        </div>
                        
                        <div class="item">
                            <img class="img-responsive" src="../images/cs6.jpg"  style="width:25%; height: 25%" >
                        </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>

            <p>Hallo, if u have any problem u can contact us by click our help below...</p>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-4">
        </div>
        <div class="col-sm-4">
            <div class="container">
                <a href="#"><img src="../images/phone.png"  style="margin-left:5.5%; margin-right:2%;" width="5%" height="5%"></a>
                <a href="https://line.me/id/"><img src="../images/lineLogo.png" style="margin:2%;" width="5%" height="5%"></a>
                <a href="https://www.whatsapp.com/?lang=id"><img src="../images/waLogo.png" style="margin:2%;" width="5%" height="5%"></a>
            </div>
        </div>
        <div class="col-sm-4">
        </div>
    </div>
    
</body>
</html>