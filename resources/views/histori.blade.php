<!DOCTYPE html>
<html>
<head>
    <title>Riwayat Pembayaran -  Auto Payment</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="/js/jquery-3.3.1.slim.min.js"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
</head>
<body style="background-image:url(/images/4.jpg);background-size:cover">
    <div class="container" style="height: 100vh">
        <div class="row-9 align-items-center justify-content-center">
        </div>
        <div class="row-9 align-items-center justify-content-center" style="height:100vh">
            <div class="col-15 login-form p-5" style="background-color: rgba(36, 30, 30, 0.51)">
                <div class="row">
                    <div class="col">
                      <p class="h1 text-light"> Riwayat Pembayaran</p>
                    </div>
                   @include('template.navbar')
                </div>
                <form method="POST" action="{{ route('login') }}">
                    <table class="table table-dark">
                        <thead>
                        <tr>
                          <th scope="col">No</th>
                          <th scope="col">Nama</th>
                          <th scope="col">Tanggal Pembayaran</th>
                          <th scope="col">Tipe Pembayaran</th>
                          <th scope="col">Harga</th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach($history as $datas)
                              <tr>
                                <th scope="row">1</th>
                                <td>{{ $datas->users->full_name }}</td>
                                <!-- <td>{{ $datas->users->tglBayar }}</td> -->
                                <td>{{$datas->tglBayar}}</td>
                                <td>{{ $datas->tagihan->instansi->namaInstansi }}</td>
                                <td>{{ $datas->tagihan->besarTagihan }}</td>
                              </tr>
                          @endforeach
                      </tbody>
                    </table>
                </form>
            </div>             
        </div>
    </div>
</body>
</html>