<!doctype html>
<html lang="{{ app()->getLocale() }}">
 
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
    <title>Pembayaran - Auto Payment</title>
 
    @yield('content1')
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <script src="../js/jquery-3.3.1.slim.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
</head>
 
<body style="background-image: url(../images/4.jpg);background-size:cover;" class="m-5">
    <div class="container" style="height: 100vh">
        <div class="row" style="height: 20vh;background-color: rgba(36, 30, 30, 0.51)">
            @yield('content3')
            <div class="col p-4 align-items-center">
                <div class="row justify-content-end">
                    @include('template.navbar')
                </div>
            </div>
        </div>
        <div class="row" style="height: 80vh;background-color: rgba(36, 30, 30, 0.51);">
            <div class="col-5" style=" margin-left: auto; margin-right: auto; margin-top: 50px">
              <div class="card">
                    <div class="card-body col-md-offset-5">
                        @yield('content2')
 
                        <form class="form-horizontal" id="donation" onsubmit="return submitForm();">

                            <input id="donor_name" name="donor_name" value="{{ \Auth::user()->full_name }}" type="hidden">

                            <input id="donor_email" name="donor_email" value="{{ \Auth::user()->email }}" type="hidden">

                            <input id="tagihan_id" class="form-control" name="tagihan_id" value={{$idTagihan}} type="hidden">

                            <input id="besar_tagihan" class="form-control" name="besar_tagihan" value="{{ App\Tagihan::where('id', $idTagihan)->get('besarTagihan')->pluck('besarTagihan')->first()}}" type="hidden">
                 
                            <button id="submit" class="btn btn-success">Bayar</button>
                 
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
 
    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script src="{{ !config('services.midtrans.isProduction') ? 'https://app.sandbox.midtrans.com/snap/snap.js' : 'https://app.midtrans.com/snap/snap.js' }}" data-client-key="{{ config('services.midtrans.clientKey') }}"></script>
    <script>
    function submitForm() {
        // Kirim request ajax
        $.post("{{ route('donation.store') }}",
        {
            _method: 'POST',
            _token: '{{ csrf_token() }}',
            tagihan_id: $('input#tagihan_id').val(),
            besar_tagihan: $('input#besar_tagihan').val(),
            donor_name: $('input#donor_name').val(),
            donor_email: $('input#donor_email').val(),
        },

        function (data, status) {
            snap.pay(data.snap_token, {
                // Optional
                onSuccess: function (result) {
                    location.reload();
                },
                // Optional
                onPending: function (result) {
                    location.reload();
                },
                // Optional
                onError: function (result) {
                    location.reload();
                }
            });
        });
        return false;
    }
    </script>
</body>
</html>