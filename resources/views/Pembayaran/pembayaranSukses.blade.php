<!DOCTYPE html>
<html>
<head>
    @yield('content1')
    <link rel="stylesheet" href="../css/bootstrap.min.css">
	<script src="../js/jquery-3.3.1.slim.min.js"></script>
	<script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>

</head>
<body style="background-image: url(images/4.jpg);background-size:cover;" class="m-5">
    <div class="container" style="height: 100vh">
        <div class="row" style="height: 20vh;background-color: rgba(36, 30, 30, 0.51)">
            <div class="col p-4 align-items-center">
			    <div class="row justify-content-end">
			        @include('template.navbar')
			    </div>
			</div>
        </div>
        <div class="row" style="height: 80vh;background-color: rgba(36, 30, 30, 0.51);">
        	<div class="col-5" style=" margin-left: auto; margin-right: auto; margin-top: 50px">
			  <div class="card">
			   		<div class="card-body col-md-offset-5">
						<h1>Pembayaran berhasil dilakukan!</h1>
					</div>
				</div>
			</div>
        </div>
    </div>
</body>
</html>
