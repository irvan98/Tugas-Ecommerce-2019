@extends('Pembayaran.pembayaran')

@section('content1')
  <title>Pembayaran listrik</title>
@stop

@section('content2')
	<h5 class="card-title">Pembayaran bulan ini sebesar Rp{{ App\Tagihan::where('id', $idTagihan)->get('besarTagihan')->pluck('besarTagihan')->first()}},00</h5>
	<h6 class="card-subtitle mb-2 text-muted">Sisa waktu yang ada {{ $sisaWaktu }} hari lagi</h6>		
@stop

@section('content3')
	<div class="col p-4">
		<h1 class="text-light">Pembayaran Kuliah</h1>
	</div>
@stop