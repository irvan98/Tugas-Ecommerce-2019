<!DOCTYPE html>

<html>

<head>
    <title>Auto Payment | Home</title>
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<script src="../jquery-3.3.1.slim.min.js"></script>
	<script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
	<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
	<link rel="stylesheet" href="../css/myCss.min.css">
	<style>
		.switch {
			position: relative;
			display: inline-block;
			width: 60px;
			height: 34px;
		}
		
		.switch input { 
			opacity: 0;
			width: 0;
			height: 0;
		}
		
		.slider {
			position: absolute;
			cursor: pointer;
			top: 0;
			left: 0;
			right: 0;
			bottom: 0;
			background-color: red;
			-webkit-transition: .4s;
			transition: .4s;
		}
		
		.slider:before {
			position: absolute;
			content: "";
			height: 26px;
			width: 26px;
			left: 4px;
			bottom: 4px;
			background-color: white;
			-webkit-transition: .4s;
			transition: .4s;
		}
		
		input:checked + .slider {
			background-color: #2196F3;
		}
		
		input:focus + .slider {
			box-shadow: 0 0 1px #2196F3;
		}
		
		input:checked + .slider:before {
			-webkit-transform: translateX(26px);
			-ms-transform: translateX(26px);
			transform: translateX(26px);
		}
		
		/* Rounded sliders */
		.slider.round {
			border-radius: 34px;
		}
		
		.slider.round:before {
			border-radius: 50%;
		}
	</style>

	<script type="text/javascript">

		// $(document).ready(function() {
		// 	console.log("masuk")
		//     $('#checkbox1').change(function() {
		//         if(this.checked) {
		//             document.getElementById("formOn").submit();
		//         }
		//         else {
		//             document.getElementById("formOff").submit();
		//         }       
		//     });
		// });
	</script>
	<script>
		function submitForm() {
        // Kirim request ajax
        $.get("{{ route('autoPayOnOff') }}",
        {
            _method: 'GET',
            onoff: $('input#onoff').val(),
        },
        return false;
    }
	</script>
	<form action="{{ route('autoPayOnOff') }}" id="formOn" onsubmit="return submitForm();">
		<input type="hidden" name="onoff" value="1" id="onoff">
		<button type="submit" style="position: absolute; top: -1000px"></button>
	</form>
	<form action="{{ route('autoPayOnOff') }}" id="formOff" onsubmit="return submitForm();">
		<input type="hidden" name="onoff" value="0" id="onoff">
		<button type="submit" style="position: absolute; top: -1000px"></button>
	</form>
</head>

<body style="background-image: url(../images/4.jpg);background-size:cover">
    
	<!--CONTAINER UTAMA-->
	<div class="container" style="background-color: rgba(36, 30, 30, 0.51)">
		
		<!--KEPALA HALAMAN WELCOME-->
        <div class="row p-3" style="">
			
			<!--JUDUL HALAMAN-->
            <div class="col">
                <h1 class="text-light">Auto Payment</h1>
            </div>
			
			<!--STATUS AUTO PAYMENT-->
			<div class="col text-right">
				<p style = "color:red;font-size:20px; display:inline;">Auto Payment is: <p id="statusAuto" style = "color:red;font-size:20px; display:inline;">
					@if(\Auth::user()->otomatis==0)
						OFF
					@else
						ON
					@endif
				</p></p>
			</div>
			
			<!--TOGGLE AUTO PAYMENT-->
			<div class="col-1 text-right">
				<label class="switch">
					<input id="checkbox1" name="checkbox" type="checkbox"  checked="true">
					<span class="slider round"></span>
					@if(\Auth::user()->otomatis==0)
						<script type="text/javascript">
							document.getElementById("checkbox1").checked=false;
						</script>
					@else
						<script type="text/javascript">
							document.getElementById("checkbox1").checked=true;
						</script>
					@endif
				</label>
			</div>
			
			<!--TOMBOL LOGOUT-->
			<div class="col-2 text-right">	
				<a class="btn btn-primary" href="{{url('/logout')}}">Keluar</a>
			</div>
        </div>
		
		<!--WELCOME *nama-orang-yang-tadi-login-->
		<div class="row p-3">
			<div class="col">
				@if(isset(Auth::user()->username))
					<p class="alert alert-success">Welcome {{Auth::user()->full_name}}</p>
				@endif
			</div>
		</div>
		
		<!--ISI HALAMAN WELCOME-->
        <div class="row p-3" style="">
		
			<!--KOLOM SIDEBAR BUTTON-->
            <div class="col-2 justify-content-center">
				<br>
                <a href="{{ route('tagihanBaru2') }}" class="btn btn-primary col text-light">Buat Tagihan Baru</a>
                <br>
				<br>
                <a href="{{ route('history') }}" class="btn btn-primary col text-light">Riwayat</a>
                <br>       
				<br>
                <a href="{{ route('berita') }}" class="btn btn-primary col text-light">Berita</a>
                <br>
                <br>
				<a class="btn btn-primary col text-light" href="{{url('/customerService')}}">Pelayanan Kustomer</a>
                <br>
                <br>
                <a href="{{ url('/help') }}" class="btn btn-primary col text-light">Bantuan</a>
                <br>
            </div>
			
			<!--KOLOM REMINDER-->
			<div class="col">
				<ul class="list-group">
					@if ($tagihan->count() > 0)
                        @foreach ($tagihan as $tagihans)
							<?php $diff_in_days = $timenow->diffInDays($tagihans->batasTgl)?>
                        	@if ($tagihans->jenis_tagihan_id == 1)
                        	<form action="{{ url('/paymentSchool/') }}">
                        		<input type="hidden" name="sisaWaktu" value="{{ $diff_in_days }}">
								@if (\Auth::user()->otomatis==1)
									<button type="submit" class="list-group-item list-group-item-action list-group-item-success btn btn-primary btn-lg disabled" name="tagihan_id" value="{{$tagihans->id}}" >
								@elseif ($diff_in_days <= 3)									
									<button type="submit" class="list-group-item list-group-item-action list-group-item-danger" name="tagihan_id" value="{{$tagihans->id}}">
								@else
									<button type="submit" class="list-group-item list-group-item-action list-group-item-warning" name="tagihan_id" value="{{$tagihans->id}}">
								@endif
										<div class="d-flex w-100 justify-content-between">
											<h5 class="mb-1">Tagihan Kuliah {{ $tagihans->instansi->namaInstansi }}</h5>
											@if($timenow < $tagihans->batasTgl)
												<small>{{ $diff_in_days = $timenow->diffInDays($tagihans->batasTgl) }} hari lagi</small>
											@elseif($timenow == $tagihans->batasTgl)

											@else
												<small>Pembayaran terlambat {{ $diff_in_days = $timenow->diffInDays($tagihans->batasTgl) }} hari</small>
											@endif
											
										</div>
										<p class="mb-1"><b>Keterangan</b> Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
										<p><b>Total Biaya </b>{{ $tagihans->besarTagihan }}</p>
									</button>
							</form>
                        	@elseif ($tagihans->jenis_tagihan_id == 2)
                        	<form action="{{ url('/paymentElectric/') }}">
                        		<input type="hidden" name="sisaWaktu" value="{{ $diff_in_days }}">
								@if (\Auth::user()->otomatis==1)
									<button type="submit" class="list-group-item list-group-item-action list-group-item-succes btn btn-primary btn-lg disabled" name="tagihan_id" value="{{$tagihans->id}}">
								@elseif ($diff_in_days <= 3)									
									<button type="submit" class="list-group-item list-group-item-action list-group-item-danger" name="tagihan_id" value="{{$tagihans->id}}">
								@else
									<button type="submit" class="list-group-item list-group-item-action list-group-item-warning" name="tagihan_id" value="{{$tagihans->id}}">
								@endif
										<div class="d-flex w-100 justify-content-between">
											<h5 class="mb-1">Tagihan Listrik</h5>
											@if($timenow <= $tagihans->batasTgl)
												<small>{{ $diff_in_days = $timenow->diffInDays($tagihans->batasTgl) }} hari lagi</small>
											@else
												<small>Pembayaran terlambat {{ $diff_in_days = $timenow->diffInDays($tagihans->batasTgl) }} hari</small>
											@endif
										</div>
										<p class="mb-1"><b>Keterangan</b> Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
										<p><b>Total Biaya </b>{{ $tagihans->besarTagihan }}</p>
									</button>
							</form>
							@elseif ($tagihans->jenis_tagihan_id == 3)
							<form action="{{ url('/paymentTelephone/') }}">
								<input type="hidden" name="sisaWaktu" value="{{ $diff_in_days }}">
								@if (\Auth::user()->otomatis==1)
									<button type="submit" class="list-group-item list-group-item-action list-group-item-success btn btn-primary btn-lg disabled" name="tagihan_id" value="{{$tagihans->id}}">
								@elseif ($diff_in_days <= 3)
									<button type="submit" class="list-group-item list-group-item-action list-group-item-danger" name="tagihan_id" value="{{$tagihans->id}}">
								@else
									<button type="submit" class="list-group-item list-group-item-action list-group-item-warning" name="tagihan_id" value="{{$tagihans->id}}">
								@endif
										<div class="d-flex w-100 justify-content-between">
											<h5 class="mb-1">Tagihan Telepon</h5>
											@if($timenow <= $tagihans->batasTgl)
												<small>{{ $diff_in_days = $timenow->diffInDays($tagihans->batasTgl) }} hari lagi</small>
											@else
												<small>Pembayaran terlambat {{ $diff_in_days = $timenow->diffInDays($tagihans->batasTgl) }} hari</small>
											@endif
										</div>
										<p class="mb-1"><b>Keterangan</b> Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
										<p><b>Total Biaya </b>{{ $tagihans->besarTagihan }}</p>
									</button>
							</form>
                        	@endIf
                        		
                        @endforeach
                     @else
                     	</br>
                    @endif

				</ul>
    		</div>
			
			<!--GW GA TAU INI APA, SIAPAPUN TOLONG JELASIN :(((-->
            <div class="col-9">
                @if(session('success'))
                    <p class="alert alert-success">{{session('success')}}</p>
                @endif
            </div>
        </div>
		
    </div>
	
</body>
<script>
	var checkbox = document.querySelector("input[name=checkbox]");
	var statusAuto = document.getElementById("statusAuto");

	checkbox.addEventListener( 'change', function() {
		if(this.checked) {
			// Checkbox is checked..
			statusAuto.innerHTML = "ON";
			document.getElementById("formOn").submit();
		} else {
			// Checkbox is not checked..
			statusAuto.innerHTML = "OFF";
			document.getElementById("formOff").submit();
		}
	});
</script>

</html>
