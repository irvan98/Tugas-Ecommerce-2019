<!DOCTYPE html>

<html>

<head>
    <title>Daftar Tagihan Baru</title>
	@include('template.header')
</head>

<body style="background-image: url(images/4.jpg);background-size:cover">
    
	<!--CONTAINER UTAMA-->
	<div class="container" style="background-color: rgba(36, 30, 30, 0.51)">
		
		<!--KEPALA HALAMAN WELCOME-->
        <div class="row p-3" style="">
            <div class="col">
                <h1 class="text-light">Auto Payment</h1>
            </div>
            <div class="col text-right">
				<!-- <a class="btn btn-primary" href="#">Pengaturan</a> -->
				<a class="btn btn-primary" href="{{url('/logout')}}">Keluar</a>				
            </div>
        </div>
		
		<!--ISI HALAMAN WELCOME-->
        <div class="row" style="">
            <!--KOLOM SIDEBAR BUTTON-->
            <div class="col-2 justify-content-center">
                <br>
                <a href="{{ route('landingpage') }}" class="btn btn-primary col text-light">Back to Home</a>
                <br>
            </div>
			
			<!--KOLOM Content-->
			<div class="col-10">
                <!-- id, history_pembayaran_id, users_id, besarTagihan, batasTgl, jenis_tagihan_id, instansi_id, 
                created_at, updated_at -->
                <form method="POST" action="{{ route('tambahTagihan') }}">
                    {{csrf_field()}}
                    
                    <div class="form-group">
                        <select name="jenis_tagihan_id">
                            @foreach ($jenisTagihan as $jenis )
                                <option value="{{$jenis->id}}">{{$jenis->deskripsiTagihan}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <select name="instansi_id">
                            @foreach ($instansi as $ins )
                                <option value="{{$ins->id}}">{{$ins->namaInstansi}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="text-light" for="besarTagihan">Besar Tagihan (RP)</label>
                        <input type="number" name="besarTagihan" class="form-control" placeholder="nominal">
                    </div>

                    <div class="form-group">
                        <label class="text-light" for="batasTgl">Batas Tanggal</label>
                        <input type="date" name="batasTgl" class="form-control">
                    </div>

                    
                    <button type="submit" class="btn btn-primary">Add New Tagihan</button>
                </form>
            </div>

        </div>
		
    </div>
			
</body>
<script>
	// var checkbox = document.querySelector("input[name=checkbox]");
	// var statusAuto = document.getElementById("statusAuto");

	// checkbox.addEventListener( 'change', function() {
	// 	if(this.checked) {
	// 		// Checkbox is checked..
	// 		statusAuto.innerHTML = "ON";
	// 	} else {
	// 		// Checkbox is not checked..
	// 		statusAuto.innerHTML = "OFF";
	// 	}
	});
</script>

</html>
