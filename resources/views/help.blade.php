<!DOCTYPE html>

<html>

<head>
	<title>Help - Auto Payment</title>
	@include('template.header')
</head>

<body style="background-image: url(images/4.jpg);background-size:cover">
	
	<!--CONTAINER-->
	<div class="container" style="height: 100vh; background-color: rgba(36, 30, 30, 0.51)">
		
		<!--HEADER-->
		<div class="row" style="height: 20vh;">
			
			<!--JUDUL HALAMAN-->
			<div class="col p-4">
				<h1 class="text-light">Help</h1>
			</div>
			
			<!--NAVBAR-->
			<div class="col p-4 align-items-center">
				<div class="row justify-content-end">
					@include('template.navbar')
				</div>
			</div>
		
		</div>
	
		<!--KELOMPOK BANTUAN-->
		<div class="panel-group" id="accordion">
			<div id="accordion">
				
				<!--BANTUAN 1-->
				<div class="card">
					<div class="card-header" id="headingOne">
						<h5 class="mb-0">
							<button class="btn btn-link list-group-item-action" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Bagaimana cara membayar?</button>
						</h5>
					</div>
					<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
						<div class="card-body">
							<ol>
								<li>Login</li>
								<li>Klik tombol pembayaran</li>
							</ol>
						</div>
					</div>
				</div>
				
				<!--BANTUAN 2-->
				<div class="card">
					<div class="card-header" id="headingTwo">
						<h5 class="mb-0">
							<button class="btn btn-link collapsed list-group-item-action" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Bagaimana jika sudah membayar tetapi tidak ada konfirmasi?</button>
						</h5>
					</div>
					<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
						<div class="card-body">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						</div>
					</div>
				</div>
				
				<!--BANTUAN 3-->
				<div class="card">
					<div class="card-header" id="headingThree">
						<h5 class="mb-0">
							<button class="btn btn-link collapsed list-group-item-action" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Pertanyaan lainnya?</button>
						</h5>
					</div>
					<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
						<div class="card-body">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						</div>
					</div>
				</div>
				
			</div>
		</div>
		
	</div>
</body>

</html>